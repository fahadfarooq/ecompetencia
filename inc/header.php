<?php 
include 'connection.php';
session_start();
if(!isset($_SESSION['user']) &&  basename($_SERVER['PHP_SELF']=='login'))
{
    header("location:login.php");
}
if(isset($_POST['logout']))
{
 session_destroy();
 header("location:login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="inc/style.css">
    <title>Document</title>
</head>
<body>